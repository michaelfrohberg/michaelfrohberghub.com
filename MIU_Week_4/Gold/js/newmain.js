$(document).on("pageinit", function () {
    var myForm = $('#enterForm'),
        formErrorsLink = $('#formErrorsLink');
        myForm.validate({
            invalidHandler: function (myForm, validator) {
                formErrorsLink.click();
                var html = '';
                for (var key in validator.submitted) {
                    var label = $('label[for^="' + key + '"]').not('[generated]');
                    var legend = label.closest('fieldset').find('.ui-controlgroup-label');
                    var fieldName = legend.length ? legend.text() : label.text();
                    html += '<li>' + fieldName + '</li>';
                }
                $("#formErrors ul").html(html);
            }, 
            // End of Invalid Handler
            submitHandler: function () {
                var data = myForm.serializeArray();
                storeData(data);
                 }
        });
        function ge( x ){
          var theElement = document.getElementById( x );
          return theElement;
        }              
        function getRadioValue() {
          var radios = document.forms[0].code;
          for (var i = 0; i < radios.length; i++) {
            if (radios[i].checked) {
              worstLang = radios[i].value;
            }
            return radios;          
            }
        }
        function getCheckBox() {
          if (ge("checkBox").checked) {
            needHelp = ge("checkBox").value;
          } else {
            needHelp = "None";
          }
        }
        function storeData(data, key) {
          if (!key) {
            var id = Math.floor(Math.random() * 1000001);
          } else {
            id = key;
          }
          getRadioValue();
          getCheckBox();
          var item = {};
            item.group = ["Group:", ge('groups').value];
            item.fname = ["First Name:", ge("fName").value];
            item.lname = ["Last Name:", ge("lName").value];
            item.phone = ["Phone #:", ge("phone").value];
            item.email = ["Email:", ge("email").value];
            item.date = ["Project Due Date:", ge("date").value];
            item.radios = ["Weakest Language:", worstLang];
            item.box = ["Needs Help:", needHelp];
            item.details = ["Project Description:", ge("notes").value];
            item.range = ["Comfort Level:", ge("range").value];
            localStorage.setItem(id, JSON.stringify(item));
            alert("Report Saved!");
          }
          function autoFillData() {
            for (var n in json) {
              var id = Math.floor(Math.random() * 1000001);
              localStorage.setItem(id, JSON.stringify(json[n]));
            }
          }
          function getData () {
            if (localStorage.length === 0) {
              alert("No data in local storage. Default form values were added.");
              autoFillData();
            }
            var makeDiv = document.createElement("div");
            makeDiv.setAttribute("id", "items");
            var makeList = document.createElement("ul");
            makeDiv.appendChild(makeList);
            document.body.appendChild(makeDiv);
            ge("items").style.display = "block";
            for (var i = 0, len = localStorage.length; i < len; i++) {
              var makeLi = document.createElement("li");
              makeLi.setAttribute("class", "list");
              var linksLi = document.createElement("li");
              makeList.appendChild(makeLi);
              var key = localStorage.key(i);
              var value = localStorage.getItem(key);
//Converting local storage string into an object
              var object = JSON.parse(value);
              var makeSubList = document.createElement("ul");
              makeLi.appendChild(makeSubList);
              getImage(object.group[1], makeSubList);
              for (var n in object) {
              var makeSubLi = document.createElement("li");
              makeSubList.setAttribute("class", "things");
              makeSubList.appendChild(makeSubLi);
              var optSubText = object[n][0] + " " + object[n][1];
              makeSubLi.innerHTML = optSubText;
              makeSubList.appendChild(linksLi);
              }
      //create edit and delete links for local storage
              makeItemLinks(localStorage.key(i), linksLi);
                  }
            }
           function getImage (catName, makeSubList) {
              var imageLi = document.createElement("li");
              makeSubList.appendChild(imageLi);
              var newImg = document.createElement("img");
              var setSrc = newImg.setAttribute("src", "images/" + catName + ".png");
              imageLi.appendChild(newImg); 
            }            
            function editItem() {
              var value = localStorage.getItem(this.key);
              var item = JSON.parse(value);
              ge("groups").value = item.group[1];
              ge("fName").value = item.fname[1];
              ge("lName").value = item.lname[1];
              ge("phone").value = item.phone[1];
              ge("email").value = item.email[1];
              ge("date").value = item.date[1];
              ge("notes").value = item.details[1];
              ge("range").value = item.range[1];
              var radios = document.forms[0].code;
              for (var i = 0; i < radios.length; i++) {
                if (radios[i].value == "HTML" && item.radios[1] == "HTML") {
                  radios[i].setAttribute("checked", "checked");
                } else if (radios[i].value == "CSS" && item.radios[1] == "CSS") {
                  radios[i].setAttribute("checked", "checked");
                } else if (radios[i].value == "JavaScript" && item.radios[1] == "JavaScript") {
                  radios[i].setAttribute("checked", "checked");
                } else if (radios[i].value == "ObjectiveC" && item.radios[1] == "ObjectiveC") {
                  radios[i].setAttribute("checked", "checked");
                } else if (radios[i].value == "Java" && item.radios[1] == "Java") {
                  radios[i].setAttribute("checked", "checked");
                }
              }
              var box = document.forms[0].checkBox;
              if (item.box[1] == "Yes") {
               ge("checkBox").setAttribute("checked","checked");
              }
      //remove save input initial event listener
              save.removeEventListener("click", storeData);
                  //change submit button value to edit  to edit
              ge("submit").val() = "Edit Report";
              var editSubmit = ge("submit");
                  //key value established as property of editSubmit
                  //so it can be used when data is saveData
              editSubmit.addEventListener("click", validate);
              editSubmit.key = this.key;
            }
            function deleteItem () {
              var ask = confirm("Are you positive you want to delete this entry?");
                if (ask) {
                  localStorage.removeItem(this.key);
                  alert("Entry deleted.")
                  window.location.reload();
                } else {
                  alert("Entry was not deleted.");
                }
              }
              //calling the json object, using it to populate empty forms	
              function makeItemLinks (key, linksLi) {
                  var editLink = document.createElement("a");
                  editLink.href = "#";
                  editLink.key = key;
                  var editText = "Edit Report";
                  editLink.addEventListener("click", editItem);
                  editLink.innerHTML = editText;
                  linksLi.appendChild(editLink);
                  var breakTag = document.createElement("br");
                  linksLi.appendChild(breakTag);
                  var deleteLink = document.createElement("a");
                  deleteLink.href = "#";
                  deleteLink.key = key;
                  var deleteText = "Delete Report";
                  deleteLink.addEventListener("click", deleteItem);
                  deleteLink.innerHTML = deleteText;
                  linksLi.appendChild(deleteLink);
              }
              // creates edit and delete links
              function clearLocal() {
                  if (localStorage.length === 0) {
                    alert("Nothing to Clear!");
                  } else {
                    localStorage.clear();
                    alert("Everything is Deleted!");
                    window.location.reload();
                    return false;
                  }
              }
              var displayData = ge("display");
              displayData.addEventListener("click", getData);
              var clearData = ge("clear");
              clearData.addEventListener("click", clearLocal);
              var save = ge("submit");
              save.addEventListener("click", storeData);
             
      });
