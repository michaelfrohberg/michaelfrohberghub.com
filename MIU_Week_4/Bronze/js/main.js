window.addEventListener("DOMContentLoaded", function () {
  function $(x) {
    var theValue = document.getElementById(x);
    return theValue;
  }
  //Building select element with options
  
  function makeCats() {
    var formTag = document.getElementsByTagName("form"),
      selectDiv = $("select"),
      makeSelect = document.createElement("select");
    makeSelect.setAttribute("id", "groups");
    for (var i = 0, j = proLang.length; i < j; i++) {
      var createOption = document.createElement("option");
      var optText = proLang[i];
      createOption.setAttribute("value", optText);
      createOption.innerHTML = optText;
      makeSelect.appendChild(createOption);
    }
    selectDiv.appendChild(makeSelect);
  }
  function getRadioValue() {
    var radios = document.forms[0].code;
    for (var i = 0; i < radios.length; i++) {
      if (radios[i].checked) {
        worstLang = radios[i].value;
      }
    }
  }
  function getCheckBox() {
    if ($("checkBox").checked) {
      needHelp = $("checkBox").value;
    } else {
      needHelp = "None";
    }
  }
  function toggleControls(n) {
    switch (n) {
      case "on":
        $("enterForm").style.display = "none";
        $("clear").style.display = "inline";
        $("display").style.display = "none";
        $("addNew").style.display = "inline";
        break;
      case "off":
        $("enterForm").style.display = "block";
        $("clear").style.display = "inline";
        $("display").style.display = "none";
        $("addNew").style.display = "inline";
        $("items").style.display = "none";
        break;
      default:
        return false;
    }
  }
  function saveData(key) {
    if (!key) {
      var id = Math.floor(Math.random() * 1000001);
    } else {
      id = key;
    }
    getRadioValue();
    getCheckbox();
    var item = {};
    item.group = ["Group:", $("groups").value];
    item.fname = ["First Name:", $("fName").value];
    item.lname = ["Last Name:", $("lName").value];
    item.phone = ["Phone #:", $("phone").value];
    item.email = ["Email:", $("email").value];
    item.date = ["Project Due Date:", $("date").value];
    item.radios = ["Weakest Language:", worstLang];
    item.box = ["Needs Help:", needHelp];
	item.details = ["Project Description:", $("describe").value];
    item.range = ["Comfort Level:", $("range").value];
    localStorage.setItem(id, JSON.stringify(item));
    alert("Report Saved!");
  }
  function autoFillData() {
    for (var n in json) {
      var id = Math.floor(Math.random() * 1000001);
      localStorage.setItem(id, JSON.stringify(json[n]));
    }
  }
  function getData() {
    toggleControls("on");
    if (localStorage.length === 0) {
      alert("No data in local storage. Default form values were added.");
      autoFillData();
    }
    var makeDiv = document.createElement("div");
    makeDiv.setAttribute("id", "items");
    var makeList = document.createElement("ul");
    makeDiv.appendChild(makeList);
    document.body.appendChild(makeDiv);
    $("items").style.display = "block";
    for (var i = 0, len = localStorage.length; i < len; i++) {
      var makeLi = document.createElement("li");
      makeLi.setAttribute("class", "list");
      var linksLi = document.createElement("li");
      makeList.appendChild(makeLi);
      var key = localStorage.key(i);
      var value = localStorage.getItem(key);
      //Converting local storage string into an object
      var object = JSON.parse(value);
      var makeSubList = document.createElement("ul");
      makeLi.appendChild(makeSubList);
      getImage(object.group[1], makeSubList);
      for (var n in object) {
        var makeSubLi = document.createElement("li");
        makeSubList.setAttribute("class", "things");
        makeSubList.appendChild(makeSubLi);
        var optSubText = object[n][0] + " " + object[n][1];
        makeSubLi.innerHTML = optSubText;
        makeSubList.appendChild(linksLi);
      }
      //create edit and delete links for local storage
      makeItemLinks(localStorage.key(i), linksLi);
    }
  }
  function getImage(catName, makeSubList) {
    var imageLi = document.createElement("li");
    makeSubList.appendChild(imageLi);
    var newImg = document.createElement("img");
    var setSrc = newImg.setAttribute("src", "images/" + catName + ".png");
    imageLi.appendChild(newImg);
  }
  function editItem() {
    var value = localStorage.getItem(this.key);
    var item = JSON.parse(value);
    toggleControls("off");
    $("groups").value = item.group[1];
    $("fName").value = item.fname[1];
    $("lName").value = item.lname[1];
    $("phone").value = item.contact[1];
    $("email").value = item.email[1];
    $("date").value = item.date[1];
    $("describe").value = item.details[1];
    $("range").value = item.range[1];
    var radios = document.forms[0].code;
    for (var i = 0; i < radios.length; i++) {
      if (radios[i].value == "HTML" && item.radios[1] == "HTML") {
        radios[i].setAttribute("checked", "checked");
      } else if (radios[i].value == "CSS" && item.radios[1] == "CSS") {
        radios[i].setAttribute("checked", "checked");
      } else if (radios[i].value == "JavaScript" && item.radios[1] == "JavaScript") {
        radios[i].setAttribute("checked", "checked");
      } else if (radios[i].value == "ObjectiveC" && item.radios[1] == "ObjectiveC") {
        radios[i].setAttribute("checked", "checked");
      } else if (radios[i].value == "Java" && item.radios[1] == "Java") {
        radios[i].setAttribute("checked", "checked");
      }
    }
    var box = document.forms[0].checkBox;
    if (item.box[1] == "Yes") {
      $("checkBox").setAttribute("checked", "checked");
    }
    //remove save input initial event listener
    save.removeEventListener("click", saveData);
    //change submit button value to edit  to edit
    $("submit").value = "Edit Report";
    var editSubmit = $("submit");
    //key value established as property of editSubmit
    //so it can be used when data is saveData
    editSubmit.addEventListener("click", validate);
    editSubmit.key = this.key;
  }
  function deleteItem() {
    var ask = confirm("Are you positive you want to delete this entry?");
    if (ask) {
      localStorage.removeItem(this.key);
      alert("Entry deleted.")
      window.location.reload();
    } else {
      alert("Entry was not deleted.");
    }
  }
  //calling the json object, using it to populate empty forms	
  function makeItemLinks(key, linksLi) {
    var editLink = document.createElement("a");
    editLink.href = "#";
    editLink.key = key;
    var editText = "Edit Report";
    editLink.addEventListener("click", editItem);
    editLink.innerHTML = editText;
    linksLi.appendChild(editLink);
    var breakTag = document.createElement("br");
    linksLi.appendChild(breakTag);
    var deleteLink = document.createElement("a");
    deleteLink.href = "#";
    deleteLink.key = key;
    var deleteText = "Delete Report";
    deleteLink.addEventListener("click", deleteItem);
    deleteLink.innerHTML = deleteText;
    linksLi.appendChild(deleteLink);
  }
  // creates edit and delete links
  function clearLocal() {
    if (localStorage.length === 0) {
      alert("Nothing to Clear!");
    } else {
      localStorage.clear();
      alert("Everything is Deleted!");
      window.location.reload();
      return false;
    }
  }
  function validate(e) {
    var getGroup = $("groups");
    var getFiName = $("fName");
    var getLaName = $("lName");
    var getPhone = $("phone");
    var getEmail = $("email");
    var getDate = $("date");
    var getDetails = $("describe")
    //error reset
    errMsg.innerHTML = "";
    getGroup.style.border = "1px solid black";
    getFiName.style.border = "1px solid black";
    getLaName.style.border = "1px solid black";
    getPhone.style.border = "1px solid black";
    getEmail.style.border = "1px solid black";
    getDate.style.border = "1px solid black";
    getDetails.style.border = "1px solid black";
    // get error messages
    var messageAry = [];
    if (getGroup.value === "") {
      var langError = "Please enter your favorite programming language";
      getGroup.style.border = "1px solid red";
      messageAry.push(langError);
    }
    if (getFiName.value === "") {
      var firstNameError = "Please enter your first name.";
      getFiName.style.border = "1px solid red";
      messageAry.push(firstNameError);
    }
    if (getLaName.value === "") {
      var lastNameError = "Please enter your last name.";
      getProduction.style.border = "1px solid red";
      messageAry.push(lastNameError);
    }
    if (getPhone.value === "") {
      var phoneError = "Please enter a production phone number.";
      getPhone.style.border = "1px solid red";
      messageAry.push(phoneError);
    }
    if (getDetails.value === "") {
      var detailError = "Please enter a production phone number.";
      getDetails.style.border = "1px solid red";
      messageAry.push(detailError);
    }
    // 	email
    var re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!(re.exec(getEmail.value))) {
      var emailError = "Please enter a valid email address.";
      getEmail.style.border = "1px solid red";
      messageAry.push(emailError);
    }
    if (getDate.value === "") {
      var getDateError = "Please enter a date.";
      getDate.style.border = "1px solid red";
      messageAry.push(getDateError);
    }
    //		display any errors on screen
    if (messageAry.length >= 1) {
      for (var i = 0, j = messageAry.length; i < j; i++) {
        var txt = document.createElement("li");
        txt.innerHTML = messageAry[i];
        errMsg.appendChild(txt);
      }
      e.preventDefault();
      return false;
    } else {
      saveData(this.key);
    }
  }
  // variables
  var proLang = ["Choose...", "HTML", "CSS", "JavaScript", "Objective-C", "Java"],
    worstLang,
    needHelp = "None",
   	errMsg = $("errors");
  makeCats();
  // Submit and Edit Buttons	
  var displayData = $("display");
  displayData.addEventListener("click", getData);
  var clearData = $("clear");
  clearData.addEventListener("click", clearLocal);
  var save = $("submit");
  save.addEventListener("click", validate);
});