var parseMyForm = function(data) {
  console.log(data);
};

$(document).bind("pageinit", function() {
    
    var myForm = $('#enterForm'),
        formErrorsLink = $('#formErrorsLink')
        
        ;
    
    myForm.validate({
      invalidHandler: function(form, validator) {
        formErrorsLink.click();
          var html = '';
          for(var key in validator.submitted) {
            var label = $('label[for^="'+ key +'"]');
            var legend = label.closest('fieldset').find('.ui-controlgroup-label');
            var fieldName = legend.length ? legend.text() : label.text();
            html += '<li>'+ fieldName +'</li>';
          };
          $("#formErrors ul").html(html);
      },
      submitHandler: function() {
        var data =  myForm.serializeArray();
        parseMyForm();
      }
    });
  });

        
